import requests
import json
from sys import argv
import mysql
import sys, mysql.connector, os
import re
import time
import re , time, datetime, time, string, unidecode
from time import sleep


# def productForCrawl(schedule_type):
#     cursor.execute("SELECT id FROM tasks WHERE task_type_id='keyword' AND scheduling_type_id='"+schedule_type+"' AND DATE(NOW()) >= crawl_start AND DATE(NOW()) <= crawl_end ;")
#     result = cursor.fetchall()
#     print(result)
#     listStore = list(set(result))
#     print(listStore)
#     if listStore != None:
#         for store in listStore:
#             scheduleID = saveSchedule(store[0])
#             print(scheduleID)
#             print(store[0])
#             nama = getWord(store[0])
#             print(nama[5])
#             # if url[2] == 'tokopedia':
#             #     crawlingTokped(url[0], url[3], store[0])
#             if nama[2] == 'keyword':
#                 get_keyword(nama[5], scheduleID,urlID='keyword') 

# def saveSchedule(taskID):
#     cursor.execute("INSERT INTO schedules (task_id, created_at, updated_at) VALUES ('"+str(taskID)+"', NOW(), NOW())")
#     connection.commit()
#     return cursor.lastrowid

# def getWord(taskID):
#     cursor.execute("SELECT * FROM tasks WHERE id='"+str(taskID)+"'")
#     result = cursor.fetchone()
#     return result

def get_keyword(category, scheduleID, categoryID):
    
    for i in range(11):
        headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3,text/plain',
        'accept-encoding':'gzip, deflate, br, identity, *,compress',
        'accept-language': 'en-GB,en;q=0.9,en-US;q=0.8,id;q=0.7,ms;q=0.6',
        'cache-control':'max-age=0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36',
        'Content-Type': 'application/json'}

        response = requests.get("https://shopee.co.id/api/v2/search_items/?by=relevancy&limit=50&match_id=157&newest=0&order=desc&page_type=search&version=2",headers=headers)
        response = response.json()
        result = json.loads(json.dumps(response))
        result = result["items"]
        # print(result[1]["itemid"])
        for i in range(len(result)):
            shopid = result[i]["shopid"]
            itemid = result[i]["itemid"]
            # print(i)
            # print(shopid,itemid)
            get_product(shopid, itemid, scheduleID, categoryID)
            # parseProduct(scheduleID,items["shopid"],items["itemid"],urlID,items["name"],items["price"],items["price_before_discount"],items["image"],items["item_rating"],items["historical_sold"],items["ctime"],items["stock"])

def get_product(shopID, itemID, scheduleID, categoryID):
  
    response = requests.get(url='https://shopee.co.id/api/v2/item/get?itemid='+str(itemID)+'&shopid='+str(shopID))
    parseProduct(response.json(), shopID, itemID, scheduleID, categoryID)

def parseProduct(response, shopID, itemID,  scheduleID,keywordID):
    result = json.loads(json.dumps(response["item"]))

    name  = result["name"]
    name = unidecode.unidecode(name)
    print(name)
    price = result["price"] / 100000
    # print(price)
    price_discount = result["price_before_discount"] / 100000
    # print(price_discount)
    image = "https://cf.shopee.co.id/file/"+result["image"]
    # print(image)
    rate = json.loads(json.dumps(result["item_rating"]))
    # print(rate)
    rating = round(rate["rating_star"], 1)
    # print(rating)
    tReview = rate["rating_count"][0]
    # print(rating)
    sold = result["historical_sold"]
    # print(sold)
    upload_date = datetime.datetime.fromtimestamp(result["ctime"]).strftime('%Y-%m-%d').encode('utf-8').decode('utf-8')
    # print(upload_date)
    categories = json.loads(json.dumps(result["categories"]))
    # print(categories)
    category   = json.loads(json.dumps(categories[0]))["display_name"]
    # print(category)
    sub_category = json.loads(json.dumps(categories[1]))["display_name"]
    # print(sub_category)
    sub_detail   = json.loads(json.dumps(categories[2]))["display_name"]
    # print(sub_detail)
    favorite = result["liked_count"]
    stock = result["stock"]
    # print(stock)
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    # print(regex)
    link = "https://shopee.co.id/"+regex.sub('-', name).replace(" ", "-")+str("-i.")+str(shopID)+"."+str(itemID)
    # print(link)

    # saveProduct(scheduleID, shopID, itemID, keywordID, name, price, price_discount, link, image, rating, sold, tReview, category, sub_category, sub_detail, stock, favorite, upload_date)
    # print("Save Success!!")
    time.sleep(0.5)
def saveProduct(scheduleID, storeID, productID, keywordID, name, price, price_discount, url, image, rate, sold, tReview, category, sub_category, sub_detail, status_product, like, upload_date):
    cursor.execute("INSERT INTO `shopee_products` (`schedule_id`, `store_id`, `product_id`, `keyword_id`, `product_name`, `price`, `price_before_discount`, `url`, `image`, `rate`, `sold`, `transaction_success`, `total_review`,`category`, `sub_category`, `subdetail`, `status_produk`,`like`, `upload date`, `created_at`, `updated_at`) VALUES \
    ('"+str(scheduleID)+"','"+str(storeID)+"','"+str(productID)+"','"+str(keywordID)+"',%s,'"+str(price)+"','"+str(price_discount)+"','"+str(url)+"','"+str(image)+"','"+str(rate)+"','"+str(sold)+"','0','"+str(tReview)+"','"+str(category)+"','"+str(sub_category)+"','"+str(sub_detail)+"','"+str(status_product)+"','"+str(like)+"','"+str(upload_date)+"', NOW(), NOW())",[str(name)])
    connection.commit()

try:
    # connection = mysql.connector.connect\
    # (host = "localhost", user = "root", passwd ="", db = "db_digitalkita")

    connection = mysql.connector.connect(host = "localhost", user = "root", passwd ="maulana7", db = "db_crawling")
    print("db connected")

except:
    print("No connection")
    sys.exit(0)
cursor = connection.cursor()

if __name__ == '__main__':
#     # schedule_type = argv[1]
#     # keywords = argv[2]
#     # get_product('https://shopee.co.id/Samsung-Galaxy-A6-3GB-32GB-(SM-A600GZ)-i.52635036.1205308618')
#     # torexe = os.popen(r"C:\Users\CLASS-13\Desktop\Tor Browser\Browser\TorBrowser\Tor\tor.exe")
#     # keywords = (,"SWISSE CRANBERRY 25000 EFFERVESCENT",,)
#     # x=map(get_keyword,keywords,scheduleID,urlID)
#     # print(list(x))
    get_keyword("set kamar tidur",88,88)
#     # productForCrawl('daily')






# https://shopee.co.id/api/v2/item/get?itemid=503204485&shopid=36888589
