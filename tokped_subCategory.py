from sys import argv
import pprint
import asyncio
import mysql
import sys
import mysql.connector
import re
import time
import os
from pyppeteer import launch

@asyncio.coroutine
def productid(productid):
	regexp = re.compile(r'product/')
	if regexp.search(str(productid)):
		x = re.sub(r"product/", "", str(productid))
		produk = (str(x))
		return produk

@asyncio.coroutine
def delParenthes(produk):
	regexp = re.compile(r'[()]')
	if regexp.search(str(produk)):
		x = re.sub(r"[\(\[].*?[\)\]]", "", str(produk))
		# x = str(x)
		return x

@asyncio.coroutine
def locationdef(location):
		b = ' '.join(str(location).split()[:1])
		return b

@asyncio.coroutine
def regex(produk):
		regexp = re.compile(r'rb')
		if regexp.search(str(produk)):
			x = re.sub(r"rb\b", "", str(produk))
			x = x.replace(',', '.')
			x = x.replace('Followers', '')
			a = float(x) * 1000
			produk = (int(a))
			return produk
		else:
			return str(produk)

@asyncio.coroutine
def regex2(produk):
		regexp = re.compile(r'[()]')
		if regexp.search(str(produk)):
			x = re.sub(r"[()]","", str(produk))
			x = re.sub(r"Ulasan","", x)
			x = x.replace(',','.',)
			# a = float(x)*1000
			# produk = (int(a))
			return x

@asyncio.coroutine
async def regex3(produk):
		regexp = re.compile(r'rb')
		if regexp.search(str(produk)):
			x = re.sub(r"rb\b", "", str(produk))
			x = x.replace(',', '.')
			a = float(x) * 1000
			produk = (int(a))
			return produk
		else:
			return str(produk)

@asyncio.coroutine
def regexRupiah(produk):
		regexp = re.compile(r'Rp')
		if regexp.search(str(produk)):
			x = re.sub(r"Rp", "", str(produk))
			x = re.sub(r"[.]", "", str(x))
			a = x
			produk = (int(a))
			return produk
		else:
			return str(produk)

@asyncio.coroutine
def intercept_request(req):
	"""Request Filtering"""
	if req.resourceType in ['media']:
		yield from req.abort()
	else:
		yield from req.continue_()

@asyncio.coroutine
def intercept_request_two(req):
	"""Request Filtering"""
	if req.resourceType in ['media']:
		yield from req.abort()
	else:
		yield from req.continue_()

@asyncio.coroutine
def link(scheduleID,subcategory,link):
	# yield from patch_pyppeteer()
	browser = yield from launch({"headless": False, 'waitUntil': ['load', 'domcontentloaded', 'networkidle0', 'networkidle2']})
	page = yield from browser.newPage()
	yield from page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/74.0.3729.169 Chrome/74.0.3729.169 Safari/537.36')
	# yield from page.setRequestInterception(True)
	# page.on('request', intercept_request)
	print(link)
	yield from page.goto(link,timeout=100000)
	yield from page.waitFor(2000)

	for i in range(1):
		for j in range(5,50):
			try:
				url_product =  yield from page.evaluate('''() => {
				return {
				url_product: document.getElementsByClassName("css-bk6tzz e1nlzfl3")['''+str(j)+'''].getElementsByTagName('a')[0].href,
				}
				}''')
				# print(url_product)
				url_product = url_product.get('url_product')
				print(url_product)
				yield from product(scheduleID,subcategory,url_product)
			except:
				print("selesai")
				break

		# yield from page.goto(link+'?page='+str(i+1),timeout=100000)
		# yield from page.waitFor(2000)
		# try:
		# 	url_product =  yield from page.evaluate('''() => {
		# 		return {
		# 		url_product: document.getElementsByClassName("css-1bjwylw")[0].innerText,
		# 		}
		# 		}''')
		# 	url_product = url_product.get('url_product')
		# 	print(url_product)
		# except:
		# 	print("no page")
		# 	break

		

@asyncio.coroutine
def product(scheduleID,subcategory,url_product):
	# yield from patch_pyppeteer()
	# print("ini fungsi produk")
	browser = yield from launch({"headless": False, 'waitUntil': ['load', 'domcontentloaded', 'networkidle0', 'networkidle2']})
	page = yield from browser.newPage()
	yield from page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/74.0.3729.169 Chrome/74.0.3729.169 Safari/537.36')
	# yield from page.setRequestInterception(True)
	# page.on('request', intercept_request_two)
	yield from page.goto(url_product,timeout=100000)
	yield from page.evaluate("""{window.scrollBy(0,300);}""")
	yield from page.waitFor(1000)

	try:
		status_produk = yield from page.evaluate('''() => {

		return {
			status_produk: document.getElementsByClassName("alert alert-block alert-block-not-available")[0].innerText,
			}
		}''')
		status_produk = status_produk.get("status_produk")
		print("status_produk: "+status_produk)
	except:
		status_produk = "Stok Tersedia"
		print("status_produk: "+status_produk)
	#==========================================
	try:
		yield from page.evaluate("""{window.scrollBy(0,500);}""")
		product_name = yield from page.evaluate('''() => {

		return {
			product_name: document.getElementsByClassName("css-x7lc0h")[0].innerText,
			
				}
			}''')
		product_name = product_name.get("product_name")
		print("product_name: "+product_name)
	except:
		product_name = None
		print("product_name: "+product_name)
	#==========================================
	try:
		transaction = yield from page.evaluate('''() => {

		return {
			transaction: document.getElementsByClassName("css-14skbn7")[0].innerText
			
				}
			}''')
		transaction = transaction.get("transaction")
		transaction = re.search(r'\(([^)]+)', transaction).group(1)
		print("transaction: "+str(transaction))
	except:
		transaction = None
		print("transaction: "+str(transaction))
	#==========================================
	try:
		pbd = yield from page.evaluate('''() => {

		return {
			pbd: document.getElementsByClassName("css-1fs4bb1-unf-heading e1qvo2ff8")[0].innerText,
			
				}
			}''')
		pbd = pbd.get("pbd")
		if not pbd.strip():
			pbd = None
			print("pbd: "+pbd)
		else:
			pbd = yield from regexRupiah(pbd)
			print("pbd: "+pbd)
	except:
		pbd = None
		print("pbd: "+str(pbd))
	#==========================================
	try:
		discount = yield from page.evaluate('''() => {

		return {
			discount: document.getElementsByClassName("css-qq4rst-unf-label ")[0].innerText,
			
				}
			}''')
		discount = discount.get("discount")
		if not discount.strip():
			discount = None
			print("discount: "+discount)
		else:
			discount = discount
			print("discount: "+discount)
	except:
		discount = None
		print("discount: "+str(discount))
	#==========================================
	try:
		price = yield from page.evaluate('''() => {

		return {
			price: document.getElementsByClassName("css-c820vl")[0].innerHTML
			
				}
			}''')
		price = price.get("price")
		price = yield from regexRupiah(price)
		print("price: "+str(price))
	except:
		price = None
		print("price: "+str(price))
	#==========================================
	try:
		review = yield from page.evaluate('''() => {

		return {
			review: document.getElementsByClassName("[object Object]")[0].innerText
			
				}
			}''')
		#document.getElementsByClassName("css-1rcwjzb-unf-tab e10kj5n2")[1].getElementsByClassName("css-my54ly e10kj5n1")[0].innerText
		review = review.get("review")
		review = re.search(r'\(([^)]+)', review).group(1)
		print("review: "+str(review))
		# print(review)
	except:
		review = None
		print("review: "+ str(review))
	#==========================================
	try:
		view = yield from page.evaluate('''() => {

		return {
			view: document.getElementsByClassName("css-14skbn7")[0].getElementsByTagName("span")[5].innerText
			
				}
			}''')
		# view = "Test"
		view = view.get("view")
		# view = yield from regex(view)
		print("view: "+view)
	except:
		view = None
		print("view: "+str(view))
	#==========================================
	try:
		location = yield from page.evaluate('''() => {

		return {
			location:document.getElementsByClassName("css-1s83bzu")[0].innerText
				}
			}''')
		# lcoation = "Test"
		location = location.get("location")
		location = location.split()
		location = location[0]+" "+location[1]
		# location = yield from locationdef(location)
		print("location: "+ location)
	except:
		location = None
		print("location: "+str(location))
	#==========================================
	try:
		shop_id = yield from page.evaluate('''() => {

		return {
			shop_id: document.querySelector("#shop-id").getAttribute("value")
			
				}
			}''')
		shop_id = shop_id.get("shop_id")
		print("shop_id: "+shop_id)
	except:
		shop_id= None
		print("shopd_id: "+str(shop_id))
	#==========================================
	try:
		product_id = yield from page.evaluate('''() => {

		return {
			product_id: document.querySelector("head > meta:nth-child(71)").getAttribute("content")
			
				}
			}''')
		product_id = product_id.get("product_id")
		product_id = str(product_id)
		product_id = yield from productid(product_id)
		print("product_id: "+ product_id)
	except:
		product_id = None
		print("product_id"+str(product_id))
	#==========================================
	try:
		# url_product= yield from page.evaluate('''() => {

		# return {
		# 	url_product: document.querySelector("head > meta:nth-child(72)").getAttribute("content")
			
		# 		}
		# 	}''')
		url_product = url_product
		print("url_product: "+url_product)
	except:
		url_product = None
		print(url_product)
	#==========================================
	try:
		category= yield from page.evaluate('''() => {

		return {
			category: document.querySelector("#zeus-root > div > div.css-1jdotmr > div.css-1x31j2j > ol > li:nth-child(2) > a").innerText
			
				}
			}''')
		category = category.get("category")
		print("category: "+category)
	except:
		category = None
		print(category)
	#==========================================
	try:
		sub_category= yield from page.evaluate('''() => {

		return {
			sub_category: document.querySelector("#zeus-root > div > div.css-1jdotmr > div.css-1x31j2j > ol > li:nth-child(3) > a").innerText,
			
				}
			}''')
		sub_category = sub_category.get("sub_category")
		print("sub_category: "+sub_category)
	except:
		sub_category = None
		print(sub_category)
	#==========================================
	try:
		sub_category_detail = yield from page.evaluate('''() => {

		return {
			sub_category_detail: document.querySelector("#zeus-root > div > div.css-1jdotmr > div.css-1x31j2j > ol > li:nth-child(4) > a").innerText,
			
				}
			}''')
		sub_category_detail = sub_category_detail.get("sub_category_detail")
		print("sub_category_detail: "+sub_category_detail)
	except:
		sub_category_detail = None
		print(sub_category_detail)
	#==========================================
	try:
		rate = yield from page.evaluate('''() => {

		return {
			rate: document.getElementsByClassName("css-14skbn7")[0].getElementsByClassName("css-4g6ai3")[0].innerText
			
				}
			}''')
		rate = rate.get("rate")
		rate = yield from delParenthes(rate)
		print(rate)
	except:
		rate = None
		print(rate)
	#==========================================
	try:
		url_image = yield from page.evaluate('''() => {

		return {
			url_image: document.getElementsByClassName("css-1ans2w0 e18n9kgb0")[0].getElementsByTagName("img")[0].src
			
				}
			}''')
		url_image = url_image.get("url_image")
		print("url_image: "+ url_image)
	except:
		url_image = None
		print(url_image)
	#==========================================
	try:
		sold = yield from page.evaluate('''() => {

		return {
			sold: document.getElementsByClassName("css-14skbn7")[0].innerText
			
				}
			}''')
		sold =sold.get("sold")
		# sold = yield from regex3(sold)
		sold = sold.split()
		sold = sold[1]
		if sold == 'Yang':
			sold = 0
			
		print("sold: "+ sold)
	except:
		
		sold= 0
		print(sold)

	print("\n")


	# yield from saveProduct(555, 'kopi',555, 555, "test", 1000, 1000, 1000, "test", "test", 0, 0, "99%", 0, 0, "test","test","test","test","test")
	# print(scheduleID,subcategory, shop_id, product_id,product_name, pbd, discount, price, url_product, url_image, rate, sold, transaction, review, view, location,category,sub_category,sub_category_detail,status_produk)
	yield from saveProduct(scheduleID,subcategory,shop_id, product_id, product_name, pbd, discount, price, url_product, url_image, rate, sold, transaction, review, view, location,category,sub_category,sub_category_detail,status_produk)

	yield from browser.close()
	


@asyncio.coroutine
def saveProduct(scheduleID,subcategory,storeID, productID,name, pbd, discount, price, url, image, rate, sold, trx, tReview, view, location,category,sub_category,subdetail,status):
	# print("INSERT INTO tokped_products(schedule_id, store_id, product_id, url_id, product_name, price_before_discount, discount, price, url, image, rate, sold, transaction_success, total_review, view, location,category,sub_category,subdetail,status_produk,created_at, updated_at) VALUES ('"+str(scheduleID)+"','"+str(storeID)+"','"+str(productID)+"','"+str(urlID)+"',%s,'"+str(pbd)+"','"+str(discount)+"','"+str(price)+"','"+str(url)+"','"+str(image)+"','"+str(rate)+"','"+str(sold)+"','"+str(trx)+"','"+str(tReview)+"','"+str(view)+"','"+str(location)+"','"+str(category)+"','"+str(sub_category)+"','"+str(subdetail)+"','"+str(status)+"',NOW(), NOW())",[str(name)])
	cursor.execute("INSERT INTO tokped_category(schedule_id,subcategory,store_id, product_id, product_name, price_before_discount, discount, price, url, image, rate, sold, transaction_success, total_review, view, location,category,sub_category,subdetail,status_produk,created_at, updated_at) VALUES \
	('"+str(scheduleID)+"','"+str(subcategory)+"','"+str(storeID)+"','"+str(productID)+"',%s,'"+str(pbd)+"','"+str(discount)+"','"+str(price)+"','"+str(url)+"','"+str(image)+"','"+str(rate)+"','"+str(sold)+"','"+str(trx)+"','"+str(tReview)+"','"+str(view)+"','"+str(location)+"','"+str(category)+"','"+str(sub_category)+"','"+str(subdetail)+"','"+str(status)+"',NOW(), NOW())",[str(name)])
	connection.commit()
	print("Success saving products into db")
	# print(scheduleID, storeID, productID, keywordID, name, pbd, discount, price, url, image, rate, sold, trx, tReview, view, location,category,sub_category,subdetail,status)


# def runTokpedKeyword(keyword,keywordID,scheduleID):
# 	loop = asyncio.get_event_loop()
# 	loop.run_until_complete((keyword(scheduleID,keyword,keywordID)))

# try:
# 	# connection = mysql.connector.connect\
# 	# (host = "localhost", user = "root", passwd ="", db = "db_digitalkita")
# 	connection = mysql.connector.connect\
# 	(host = "localhost", user = "crawler", passwd ="crawler_g1lA", db = "digitalkita")
# except:
# 	print("No Connection")
# cursor = connection.cursor()

if __name__ == "__main__":
	# torexe = os.popen(r"C:\Users\CLASS-13\Desktop\Tor Browser\Browser\TorBrowser\Tor\tor.exe")
	try:
		connection = mysql.connector.connect(host = "localhost", user = "root", passwd ="maulana7", db = "db_crawling")
		print("db connected")
		# connection = mysql.connector.connect\
		# (host = "localhost", user = "crawler", passwd ="crawler_g1lA", db = "digitalkita")
	except:
		print("No Connection")
	cursor = connection.cursor()
	# torexe = os.popen(r"C:\Users\CLASS-13\Desktop\Tor Browser\Browser\TorBrowser\Tor\tor.exe")
	loop = asyncio.get_event_loop()
	# result = loop.run_until_complete(product(1,'https://www.tokopedia.com/lorealparis/l-oreal-lipstik-matte-color-riche-matte-248-flatter-me-nude?trkid=f=Ca2227L000P0W0S0Sh,Co0Po0Fr0Cb0_src=search_page=1_ob=23_q=lipstik_po=1_catid=2227&whid=0',1))
	# result = loop.run_until_complete(store(1,'https://www.bukalapak.com/u/yuhanacode'))
	urls = [
		'https://www.tokopedia.com/p/fashion-wanita/bawahan-wanita/celana-pendek-wanita',
		'https://www.tokopedia.com/p/fashion-wanita/bawahan-wanita/legging-wanita',
		'https://www.tokopedia.com/p/fashion-wanita/bawahan-wanita/rok-wanita',
		'https://www.tokopedia.com/p/fashion-wanita/bridal/aksesoris-pengantin',
		'https://www.tokopedia.com/p/fashion-wanita/bridal/bridesmaid-dress',
		'https://www.tokopedia.com/p/fashion-wanita/bridal/gaun-pengantin'

	]
	for i in range(6):
		loop.run_until_complete(link(92,'xxx',urls[i]+'?ob=5&page=1'))

